# Based on [Official Python image](https://hub.docker.com/_/python) & [Official Postgres image](https://hub.docker.com/_/postgres)

- `3.10.6-slim-bullseye`, `3.10-slim-bullseye`, `3-slim-bullseye`, `slim-bullseye`, `3.10.6-slim`, `3.10-slim`, `3-slim`, `slim` [(3.10.6/Dockerfile)](https://github.com/docker-library/python/blob/7b9d62e229bda6312b9f91b37ab83e33b4e34542/3.10/slim-bullseye/Dockerfile)

- `14.5-alpine`, `14-alpine`, `alpine`, `14.5-alpine3.16`, `14-alpine3.16`, `alpine3.16` [(14.5/Dockerfile)](https://github.com/docker-library/postgres/blob/56a1986772dd0f9488d54dccb82427c0db0b0599/14/alpine/Dockerfile)

## Name

Application for bewise.ai

## Descriprtion

Simple web service on base api https://jservice.io

## Install prerequisites

To run the docker commands without using **sudo** you must add the **docker** group to **your-user**:

```
sudo usermod -aG docker your-user
```

For now, this project has been mainly created for Unix `(Linux/MacOS)`. Perhaps it could work on Windows.

All requisites should be available for your distribution. The most important re :

- [Git](https://git-scm.com/downloads)
- [Docker](https://docs.docker.com/engine/installation/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Clone the project

To install [Git](http://git-scm.com/book/en/v2/Getting-Started-Installing-Git), download it and install following the instructions :

```sh
    git clone https://gitlab.com/lacosteque/bewiseai.git
```

Go to the project directory :

```sh
    cd bewiseai
```

### Project tree

```
bewiseai
├─ .dockerignore
├─ .gitignore
├─ Dockerfile
├─ bewiseai
│ ├─ **init**.py
│ └─ backend
│ ├─ api
│ │ ├─ base.py
│ │ └─ v1
│ │ ├─ route_answer.py
│ │ └─ route_question.py
│ ├─ core
│ │ ├─ config.py
│ │ └─ exceptions.py
│ ├─ db
│ │ ├─ base.py
│ │ ├─ base_class.py
│ │ ├─ models
│ │ │ └─ jserviceIO.py
│ │ ├─ repository
│ │ │ └─ jserviceIO.py
│ │ ├─ session.py
│ │ └─ utils.py
│ ├─ externalAPI
│ │ └─ jserviceIO.py
│ ├─ main.py
│ ├─ schemas
│ │ ├─ jserviceIO.py
│ │ ├─ postgres.py
│ │ ├─ request.py
│ │ └─ response.py
│ ├─ services
│ │ └─ jserviceIO.py
│ ├─ static
│ │ ├─ css
│ │ │ ├─ bootstrap-grid.min.css
│ │ │ ├─ bootstrap-grid.min.css.map
│ │ │ └─ styles.css
│ │ └─ js
│ │ └─ main.js
│ ├─ templates
│ │ ├─ components
│ │ │ ├─ answer.html
│ │ │ └─ question.html
│ │ ├─ homepage
│ │ │ └─ homepage.html
│ │ └─ shared
│ │ └─ base.html
│ ├─ utils
│ │ ├─ logger.py
│ │ └─ request.py
│ └─ webapp
│ ├─ base.py
│ └─ v1
│ └─ route_homepage.py
├─ docker-compose.yml
├─ poetry.lock
├─ pyproject.toml
└─ tests
├─ **init**.py
└─ test_bewiseai.py

```

## Run the application with Docker Compose

0. Create .env
   In the root of the project, you must create a virtual environment file .env and fill it out.

```
POSTGRES_DB="example_db"
POSTGRES_USER="example_user"
POSTGRES_PASSWORD="example_password"
```

1. Deploy & run

```sh
    docker-compose up -d
```

2. Stop

```sh
    docker-compose stop
```

3. Start

```sh
    docker-compose start
```

4. Down & remove containers | volumes

```sh
    docker-compose down
```

5. Check service API

You need to send a POST request of the form {"questions_num": integer}.
integer must be a positive number.

```sh

  curl -X 'POST' \
  'http://127.0.0.1:8000/v1/question/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "questions_num": 1
}'

```

There can be several types of responses from the API:

- If the database is empty you will get `{"question": null}`
- If the query was processed without errors `{"question": "string"}`
- If an error occurs during query execution (e.g. database or external API does not work) `{"error": "Service unavailable, try again later"`
- If the API fails to validate the input data, it gives code 422 with an error description.

6. Check web appication
   
   The service has a web application available at http://127.0.0.1:8000/

- Application start and null response

![Null response](https://gitlab.com/lacosteque/bewiseai/-/raw/main/bewiseai/screenshots/response-null.png)
Get question

![question](https://gitlab.com/lacosteque/bewiseai/-/raw/main/bewiseai/screenshots/response-question.png)

Get answer

![answer](https://gitlab.com/lacosteque/bewiseai/-/raw/4e47c4efaedc504f50fa3b8d691b6b8a78dcdf38/bewiseai/screenshots/response-answer.png)

Service error response

![error](https://gitlab.com/lacosteque/bewiseai/-/raw/main/bewiseai/screenshots/response-error.png)

## API documentation

- ReDoc - [http://127.0.0.1:8000/redoc](http://127.0.0.1:8000/redoc)
- Swagger - [http://127.0.0.1:8000/docs](http://127.0.0.1:8000/docs)
