from fastapi import APIRouter

from api.v1 import route_answer, route_question

api_router = APIRouter()
api_router.include_router(
    route_question.router, prefix='/v1', tags=['question']
)
api_router.include_router(route_answer.router, prefix='/v1', tags=['answer'])
