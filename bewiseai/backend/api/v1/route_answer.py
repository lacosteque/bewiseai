from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from db.repository.jserviceIO import fetch_answer
from db.session import get_session
from schemas.jserviceIO import GetAnswer, ShowAnswer

router = APIRouter()
endpoint = '/answer/'


@router.post(
    endpoint, response_model=ShowAnswer, response_model_exclude_unset=True
)
async def answer(
    item: GetAnswer,
    db: AsyncSession = Depends(get_session),
):
    try:
        answer = await fetch_answer(db, item)
        output = ShowAnswer(answer=answer)
    except ServiceError:
        output = ShowAnswer(error='Service unavailable, try again later')
    return output

    return output
