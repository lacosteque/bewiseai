from fastapi import APIRouter, Depends
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from core.exceptions import ServiceError
from db.session import get_session
from schemas.jserviceIO import GetQuestion, ShowQuestion
from services.jserviceIO import app_tasks
from utils.request import get_client

router = APIRouter()
endpoint = '/question/'


@router.post(
    endpoint, response_model=ShowQuestion, response_model_exclude_unset=True
)
async def question(
    item: GetQuestion,
    client: AsyncClient = Depends(get_client),
    db: AsyncSession = Depends(get_session),
):

    try:
        question = await app_tasks(client, db, item)
        output = ShowQuestion(question=question)
    except ServiceError:
        output = ShowQuestion(error='Service unavailable, try again later')
    return output
