class BadHTTPresponse(Exception):
    """Bad http response"""


class ApiServiceError(Exception):
    """API service can't current response"""


class ServiceError(Exception):
    """Service unresponsive"""


class DatabaseError(Exception):
    """Database service error"""
