from sqlalchemy import Column
from sqlalchemy.dialects.postgresql import DATE, INTEGER, JSONB, TEXT

from db.base_class import Base


class JserviceIO(Base):   # pyright: ignore
    id = Column(INTEGER, primary_key=True, index=True)
    question_id = Column(INTEGER, unique=True)
    answer = Column(TEXT)
    question = Column(TEXT)
    value = Column(INTEGER)
    airdate = Column(DATE)
    created_at = Column(DATE)
    updated_at = Column(DATE)
    category_id = Column(INTEGER)
    game_id = Column(INTEGER)
    category = Column(JSONB)
