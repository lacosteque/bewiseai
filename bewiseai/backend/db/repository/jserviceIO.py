from typing import Iterable

from sqlalchemy import select
from sqlalchemy.exc import ProgrammingError, SQLAlchemyError
from sqlalchemy.ext.asyncio import AsyncSession

from core.exceptions import DatabaseError
from db.models.jserviceIO import JserviceIO as ModelJIO
from db.utils import init_models
from utils.logger import AppLogger

log = AppLogger(__name__)


async def add_entry(model: ModelJIO, db: AsyncSession) -> None:
    try:
        db.add(model)
        await db.commit()
        await db.refresh(model)
        await log.info(
            f'Question with ID {model.question_id} has been successfully added'
        )
    except (SQLAlchemyError, OSError) as e:
        await log.error(e)
        raise DatabaseError


async def add_pool_entry(pool: Iterable, db: AsyncSession) -> None:
    try:
        db.add_all(pool)
        ids = (model.question_id for model in pool)
        await db.commit()
        models = (model for model in pool)
        ids = tuple(models.question_id for models in models)
        await log.info(
            f'Questions with IDs {", ".join(map(str, ids))} has been successfully added'
        )

    except (SQLAlchemyError, OSError) as e:
        await log.error(e)
        raise DatabaseError


async def fetch_previous_question(db: AsyncSession) -> str:
    try:

        stmt = (
            select(ModelJIO.question)
            .order_by(ModelJIO.id.desc())
            .offset(1)
            .limit(1)
        )
        rows = await db.execute(stmt)
        output = rows.scalars().first()
    except (SQLAlchemyError, OSError) as e:
        await log.error(e)
        raise DatabaseError
    return output


async def fetch_answer(db: AsyncSession, model) -> str:
    try:
        stmt = select(ModelJIO.answer).where(
            ModelJIO.question == model.question
        )
        rows = await db.execute(stmt)
        output = rows.scalars().first()
    except (SQLAlchemyError, OSError) as e:
        await log.error(e)
        raise DatabaseError
    return output


async def fetch_ids(db: AsyncSession) -> Iterable:
    try:
        stmt = select(ModelJIO.question_id)
        rows = await db.execute(stmt)
        output = rows.scalars().all()

    except ProgrammingError as e:
        if str(e).find(f'relation "{ModelJIO.__tablename__}" does not exist'):
            await init_models(True)
        await log.error(e)
        raise DatabaseError
    except (SQLAlchemyError, OSError) as e:
        await log.error(e)
        raise DatabaseError

    return output
