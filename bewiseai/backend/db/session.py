from typing import AsyncGenerator

from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

from core.config import settings
from schemas.postgres import PostgreSQL

db = PostgreSQL(url=settings.DATABASE_URL)   # pyright: ignore
engine = create_async_engine(db.url, echo=False, pool_pre_ping=True)

async_session = sessionmaker(
    engine, class_=AsyncSession, expire_on_commit=False
)


async def get_session() -> AsyncGenerator:
    async with async_session() as session:   # pyright: ignore
        yield session
