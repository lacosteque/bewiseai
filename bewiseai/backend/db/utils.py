import asyncio

from sqlalchemy import select
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.asyncio.engine import AsyncConnection

from db.base import Base
from db.session import engine
from utils.logger import AppLogger

log = AppLogger(__name__)


async def init_models(connect: bool) -> None:
    if connect:
        async with engine.begin() as conn:
            await conn.run_sync(Base.metadata.create_all)   # pyright: ignore
            await log.info('Database models init - done!')
        await engine.dispose()


async def check_db_connected(timeout: int = 3, retries: int = 10) -> bool:

    connect = await _db_connected()

    attempt = 0
    while not connect:
        attempt += 1
        if attempt == retries:
            await log.warning(
                f'{retries} - connection attempts are unsuccessful'
            )
            return False

        await log.info(f'Trying to connect to the database - {attempt}')
        await asyncio.sleep(timeout)
        connect = await _db_connected()
    return True


async def _db_connected() -> AsyncConnection | None:
    try:
        async with engine.begin() as conn:
            await conn.execute(select(1))
            await log.info('Database is connected (^_^)')
            return conn
    except (SQLAlchemyError, OSError) as e:
        await log.error(f'Database is down (*_*) - {e}')
