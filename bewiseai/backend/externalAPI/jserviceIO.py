from json.decoder import JSONDecodeError
from typing import Iterable

from httpx import AsyncClient
from pydantic import ValidationError

from core.exceptions import ApiServiceError, BadHTTPresponse
from schemas.jserviceIO import GetQuestion
from schemas.request import RequestDataHTTP
from schemas.response import JserviceIO
from utils.logger import AppLogger
from utils.request import HTTPrequest

log = AppLogger(__name__)


async def get_api_data(
    client: AsyncClient, data: GetQuestion
) -> Iterable[JserviceIO]:
    request = HTTPrequest(client)
    url = 'https://jservice.io/api/random'
    params = {'count': data.questions_num}
    requestData = RequestDataHTTP(
        method='GET', url=url, params=params  # pyright: ignore
    )

    try:
        response = await request.send(requestData)
        json = response.json()
        output = tuple(JserviceIO(**data) for data in json)
    except (BadHTTPresponse, JSONDecodeError, ValidationError, TypeError) as e:
        await log.error(e)
        raise ApiServiceError
    return output
