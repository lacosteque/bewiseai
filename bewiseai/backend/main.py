import uvicorn
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from api.base import api_router
from core.config import settings
from db.utils import check_db_connected, init_models
from webapp.base import api_router as web_router


def include_router(app):
    app.include_router(api_router)
    app.include_router(web_router)


def configure_static(app):
    app.mount('/static', StaticFiles(directory='static'), name='static')


def start_application():

    app = FastAPI(
        title=settings.PROJECT_NAME,
        version=settings.PROJECT_VERSION,
    )
    include_router(app)
    configure_static(app)

    return app


app = start_application()


@app.on_event('startup')
async def app_startup():
    connect = await check_db_connected(timeout=3, retries=30)
    await init_models(connect)


@app.on_event('shutdown')
async def app_shutdown():
    pass


if __name__ == '__main__':
    uvicorn.run('__main__:app', host='0.0.0.0', port=8000, reload=True)
