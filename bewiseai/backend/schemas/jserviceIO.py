from pydantic import BaseModel, PositiveInt


class GetQuestion(BaseModel):
    questions_num: PositiveInt


class ShowQuestion(BaseModel):
    question: str = None   # pyright: ignore
    error: str = None   # pyright: ignore

    class Config:
        orm_mode = True


class GetAnswer(BaseModel):
    question: str


class ShowAnswer(BaseModel):
    answer: str = None   # pyright: ignore
    error: str = None   # pyright: ignore

    class Config:
        orm_mode = True
