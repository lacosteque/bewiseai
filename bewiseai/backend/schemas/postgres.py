from pydantic import BaseModel, PostgresDsn


class PostgreSQL(BaseModel):
    url: PostgresDsn
