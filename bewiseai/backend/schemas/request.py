from pydantic import BaseModel, HttpUrl


class RequestDataHTTP(BaseModel):
    method: str
    url: HttpUrl
    headers: dict = None   # pyright: ignore
    get_params: dict = None  # pyright: ignore
    post_data: dict = None    # pyright: ignore
