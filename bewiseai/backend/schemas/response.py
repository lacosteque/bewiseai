import datetime
from typing import TypedDict

from pydantic import BaseModel, Field


class Category(TypedDict):
    id: int
    clues_count: int
    title: str
    created_at: str
    updated_at: str


class JserviceIO(BaseModel):
    question_id: int = Field(alias='id')
    answer: str
    question: str
    value: int = None   # pyright: ignore
    airdate: datetime.datetime
    created_at: datetime.datetime
    updated_at: datetime.datetime
    category_id: int
    game_id: int
    category: Category
