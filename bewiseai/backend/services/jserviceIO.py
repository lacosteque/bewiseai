from typing import Iterable

from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from core.exceptions import ApiServiceError, DatabaseError, ServiceError
from db.models.jserviceIO import JserviceIO as ModelJIO
from db.repository.jserviceIO import (add_entry, add_pool_entry, fetch_ids,
                                      fetch_previous_question)
from externalAPI.jserviceIO import get_api_data
from schemas.jserviceIO import GetQuestion
from schemas.response import JserviceIO
from utils.logger import AppLogger

log = AppLogger(__name__)


async def app_tasks(client: AsyncClient, db: AsyncSession, item: GetQuestion):

    try:
        response = await get_api_data(client, item)
        unique = await _check_unique_question(db, response)

        i = 0
        while unique:
            if i == 100:
                await log.warning('A lot of requests to external API')
                break
            i += 1
            response = await get_api_data(client, item)
            unique = await _check_unique_question(db, response)

        await _add_to_db(db, response, item.questions_num)
        output = await fetch_previous_question(db)

    except (ApiServiceError, DatabaseError) as e:
        raise ServiceError
    return output


async def _add_to_db(
    db: AsyncSession, list_response: Iterable[JserviceIO], qty: int
) -> None:
    if qty == 1:
        model, *_ = list_response
        await add_entry(ModelJIO(**model.dict()), db)
    else:
        pool = tuple(ModelJIO(**model.dict()) for model in list_response)
        await add_pool_entry(pool, db)


async def _check_unique_question(
    db: AsyncSession, list_response: Iterable[JserviceIO]
) -> Iterable:

    ids = await fetch_ids(db)
    response_id = (model.question_id for model in list_response)
    output = tuple(set(ids) & set(response_id))
    if output:
        await log.info(f'Duplicates detected {output}, re-request to API')
    return output
