var questions_btn = document.getElementById("questions_btn");
var value = Number(questions_btn.dataset.value);

questions_btn.onclick = function() {
  (async () => {
    try {
      let data = { questions_num: value };
      let json = JSON.stringify(data);

      var loader = document.getElementById("question_loader");
      var question_text = document.getElementById("question_text");
      var error_text = document.getElementById("error_text");
      var answer_btn = document.getElementById("answer_btn");

      loader.classList.remove("none");
      question_text.classList.add("none");
      error_text.classList.add("none");

      let response = await fetch("/v1/question/", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: json,
      });

      if (response.ok) {
        let data = await response.json();
        let question = data["question"];
        let error = data["error"];

        if (error) {

          answer_btn.disabled = true;
          error_text.innerHTML = error;
          error_text.classList.remove("none");
        }

        else {
          answer_btn.disabled = false;
          question_text.innerHTML = question;
          question_text.classList.remove("none");
        }
      }
    } catch (error) {
      console.log(error);
    }

    loader.classList.add("none");

  })();
};

document.getElementById("answer_btn").onclick = function() {
  var questions_text = document.getElementById("question_text").innerText;
  var data = { question: questions_text };
  var json = JSON.stringify(data);

  (async () => {
    try {
      var loader = document.getElementById("answer_loader");
      var text = document.getElementById("answer_text");
      loader.classList.remove("none");
      text.classList.add("none");

      let response = await fetch("/v1/answer/", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: json,
      });

      if (response.ok) {
        let data = await response.json();
        let answer = data["answer"];

        document.querySelector("#answer_text").innerHTML = answer;
      }
    } catch (error) {
      console.log(error);
    }

    loader.classList.add("none");
    text.classList.remove("none");
    document.getElementById("answer").classList.remove("none");
  })();
};
