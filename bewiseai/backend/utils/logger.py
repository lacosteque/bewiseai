import logging.config
from logging import ERROR, Logger, getLogger

from core.config import settings


class AppLogger(Logger):
    def __init__(self, name):
        self.logger = getLogger(name)
        logging.config.dictConfig(settings.LOGGING)

    async def info(self, message):
        self.logger.info(message)

    async def warning(self, message):
        self.logger.warn(message)

    async def error(self, msg, *args, **kwargs):
        self.logger._log(ERROR, msg, args, **kwargs)

    async def exception(self, msg, *args, exc_info=True, **kwargs):
        self.logger.error(msg, *args, exc_info=exc_info, **kwargs)
