from typing import AsyncGenerator

import httpx

from core.exceptions import BadHTTPresponse
from schemas.request import RequestDataHTTP
from utils.logger import AppLogger

log = AppLogger(__name__)


async def get_client() -> AsyncGenerator:
    async with httpx.AsyncClient(http2=True, timeout=30) as client:
        yield client


class HTTPrequest:
    def __init__(self, client: httpx.AsyncClient):
        self.client = client

    async def send(self, data: RequestDataHTTP) -> httpx.Response:
        try:
            response = await self.client.request(
                method=data.method,
                url=data.url,
                params=data.get_params,
                data=data.post_data,
            )
            response.raise_for_status()
        except httpx.HTTPError as exc:
            await log.error(exc)
            raise BadHTTPresponse

        return response
