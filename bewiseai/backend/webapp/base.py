from fastapi import APIRouter

from webapp.v1 import route_homepage

api_router = APIRouter()
api_router.include_router(route_homepage.router, prefix='', tags=['homepage'])
